import datetime
import os
import yaml
from netmiko import ConnectHandler


# Load config
with open('config.yaml', 'r') as file:
    config = yaml.safe_load(file)

# Define the SCP parameters
scp_parameters = config['scp_parameters']

# Define the device details
device = {
    'device_type': 'paloalto_panos',
    'ip': config['ip'],
    'username': config['username'],
    'password': config['password'],
    'global_delay_factor': 2,
    "session_log": 'netmiko_session.log',
    "fast_cli": False,
}

# start the session with the device
net_connect = ConnectHandler(**device, port=config['port'], global_cmd_verify=False)


def log_extractor(net_connect: ConnectHandler,
                  scp_parameters: dict,
                  logtype: str,
                  start_time: datetime.datetime,
                  end_time: datetime.datetime):
    """Extract the logs from the device and send them to the SCP server"""
    scp_server = scp_parameters['scp_server']
    scp_user = scp_parameters['scp_user']
    scp_password = scp_parameters['scp_password']
    scp_path = scp_parameters['scp_path']

    # Set strings with time in format 2023/05/01@00:00:00
    start_time_str = start_time.strftime("%Y/%m/%d@%H:%M:%S")
    end_time_str = end_time.strftime("%Y/%m/%d@%H:%M:%S")
    # set interval string as 20230501-000000-20230501-235959
    interval_str = (start_time + datetime.timedelta(hours=scp_parameters['offset_hrs'])).strftime("%Y%m%d_%H%M%S")
    interval_str += "-"
    interval_str += (end_time + datetime.timedelta(hours=scp_parameters['offset_hrs'])).strftime("%Y%m%d_%H%M%S")
    # Set the filename
    filename = f"{logtype}-{interval_str}.log"
    # Setup the command string
    cmd = 'scp export log traffic ' \
    f'start-time equal {start_time_str} end-time equal {end_time_str} ' \
    f'to {scp_user}@{scp_server}:{scp_path}/{filename} '
    print("Sending command: " + cmd)
    output = net_connect.send_command(cmd, expect_string="word:")

    try:
        sending_time = datetime.datetime.now()
        print(f"Password sent at {sending_time}")
        output = net_connect.send_command(scp_password,
                                          expect_string=">",
                                          delay_factor=30,
                                          read_timeout=120.0,
                                          strip_command=True)
    except Exception as e:
        exception_time = datetime.datetime.now()
        print(f"Error: Timeout at {exception_time}")
        raise e
    else:
        if "exported successfully" in output:
            print("Command executed successfully")
        else:
            print("--- Error: Command failed --- ")
            print(output)
        return scp_path + "/" + filename


def get_logfile_last_timestamp(filename: str, ts_fieldnum: int = 7) -> datetime.datetime:
    """Get the last timestamp from a log file where
    the timestamp is the 2nd field in the line and
    has the format 2023/05/01 00:00:00.
    Args:
        filename (str): _description_

    Returns:
        datetime.datetime: _description_
    """
    with open(filename, "r") as f:
        last_line = f.readlines()[-1]
    # Return None if file is empty
    if last_line == "":
        return None
    else:
        last_timestamp_str = last_line.split(',')[ts_fieldnum - 1]
        last_timestamp = datetime.datetime.strptime(last_timestamp_str, "%Y/%m/%d %H:%M:%S")
        return last_timestamp


def rename_logfile(filename: str, endtime: datetime.datetime) -> str:
    filename_parts = filename.strip('.log').split('-')
    new_filename = filename_parts[0] + "-" + filename_parts[1] + "-" + endtime.strftime("%Y%m%d_%H%M%S") + ".log"
    try:
        os.rename(filename, new_filename)
    except Exception as e:
        print(f"Error renaming file {filename} to {new_filename}")
        raise e
    else:
        return new_filename


if __name__ == "__main__":
    # filename = scp_parameters['scp_path'] + "/" + "traffic-20230430-180000-20230501-175959.log"
    # print(f"Last Timestamp for {filename}: {get_logfile_last_timestamp(filename)}")
    continue_flag = True
    start_time = datetime.datetime(2023, 5, 3, 00, 00, 00) - datetime.timedelta(hours=scp_parameters['offset_hrs'])
    end_time = start_time + datetime.timedelta(days=2)
    while continue_flag:
        filename = log_extractor(net_connect, scp_parameters, "traffic", start_time=start_time, end_time=end_time)
        last_timestamp = get_logfile_last_timestamp(filename)
        if last_timestamp is None:
            continue_flag = False
            break
        else:
            print(f"Last Timestamp for {filename}: {last_timestamp}")
            new_fname = rename_logfile(filename, last_timestamp)
            os.system(f"gzip {new_fname} &")
            if last_timestamp - datetime.timedelta(hours=scp_parameters['offset_hrs']) >= end_time:
                continue_flag = False
                break
            if last_timestamp - datetime.timedelta(hours=scp_parameters['offset_hrs']) != start_time:
                start_time = last_timestamp - datetime.timedelta(hours=scp_parameters['offset_hrs'])
            else:
                start_time = last_timestamp - datetime.timedelta(hours=scp_parameters['offset_hrs'])
                start_time += datetime.timedelta(seconds=1)
    net_connect.disconnect()
